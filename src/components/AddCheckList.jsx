import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const AddBoard = (props) => {
  const { newCheckListValue, onCheckListChange, addNewCheckList } = props;

  const [expanded, setExpanded] = useState(false);

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  const addNewCheckListItem =()=>{
    addNewCheckList()
    setExpanded(!expanded);
  }

  return (
    <Card
      sx={{
        margin: "10px",
        width: "100%",
      }}
      variant="outlined"
    >
      <CardContent>
        <Button
          onClick={handleExpand}
          sx={{
            color: "#1345a1",
            fontSize: "18px",
            fontWeight: "bold",
          }}
        >
          + Add CheckList
        </Button>
        {expanded && (
          <div>
            <TextField
              label="Enter Checklist"
              value={newCheckListValue}
              onChange={onCheckListChange}
              sx={{height: "30px"}}
            />
            <Button variant="contained" color="primary" onClick={addNewCheckListItem} sx={{marginLeft:'10px'}}>
              Add
            </Button>
          </div>
        )}
      </CardContent>
    </Card>
  );
};

export default AddBoard;
