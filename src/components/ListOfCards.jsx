import { useState, useEffect } from "react";
import { Card, CardContent, Typography, IconButton, List } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CreateCard from "./CreateCard";
import { getCards, addCards, deleteCardItem } from "./ApiCalls";
import CardItem from "./CardItem";
import LoadingPage from "./LoadingPage";

const ListOfCards = (props) => {
  const { listId, name, deleteList } = props;
  const [cards, setCards] = useState([]);
  const [newCardValue, setNewCard] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    getCards(listId)
      .then((response) => {
        setCards(response.data);
        setLoading(false);
        setError(null);
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
      });
  }, []);

  const onCardChange = (e) => {
    setNewCard(e.target.value);
  };

  const addNewCard = () => {
    addCards(newCardValue, listId)
      .then((response) => {
        setCards([...cards, response.data]);
        setNewCard("");
      })
      .catch((err) => {
        setError(err.message);
      });
  };

  const deleteCard = (cardId) => {
    deleteCardItem(cardId)
      .then((response) => {
        setCards(cards.filter((card) => card.id !== cardId));
      })
      .catch((err) => {
        setError(err.message);
      });
  };

  return (
    <Card
      sx={{
        margin: "20px",
        minWidth: 400,
        height: "fit-content",
        backgroundColor: "#858176",
      }}
      variant="outlined"
    >
      <CardContent sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography sx={{ color: "#fff" }} variant="h5">
          {name}
        </Typography>
        <IconButton onClick={() => deleteList(listId)}>
          <DeleteIcon />
        </IconButton>
      </CardContent>
      {loading ? (
        <LoadingPage />
      ) : (
        <List>
          {cards.map((card) => (
            <CardItem
              key={card.id}
              card={card}
              deleteCard={deleteCard}
              listName={name}
            />
          ))}
        </List>
      )}
      {error ? (
        <p>Error: {error}</p>
      ) : (
        <CreateCard
          newCardValue={newCardValue}
          onCardChange={onCardChange}
          addNewCard={addNewCard}
        />
      )}
    </Card>
  );
};

export default ListOfCards;
