import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const CreateList= (props) => {
  const { newListValue, onListChange, addNewList } = props;

  const [expanded, setExpanded] = useState(false);

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  return (
    <Card
      sx={{
        margin: "20px",
        minWidth: 400,
        height:150
      }}
      variant="outlined"
    >
      <CardContent>
        <Button
          onClick={handleExpand}
          sx={{
            color: "#1345a1",
            fontSize: "20px",
            fontWeight: "bold",
          }}
        >
          + Add List
        </Button>
        {expanded && (
          <div>
            <TextField
              label="Enter List Name"
              value={newListValue}
              onChange={onListChange}
            />
            <Button variant="contained" color="primary" onClick={addNewList} sx={{marginLeft:'10px'}}>
              Add
            </Button>
          </div>
        )}
      </CardContent>
    </Card>
  );
};

export default CreateList;
