import Typography from "@mui/material/Typography";
import { Box, Card, CardContent } from "@mui/material";
import { Link } from "react-router-dom";

const Board = (props) => {
  const { boardId, name, backgroundImage } = props;
  return (
    <Link to={`/boards/${boardId}`} className="link-decoration">
      <Card
        sx={{
          margin: "20px",
          width: 300,
          height: 300,
          backgroundImage,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
        variant="outlined"
      >
      <CardContent>
          <Typography variant="h5" >{name}</Typography>
        </CardContent>
      </Card>
    </Link>
  );
};

export default Board;
