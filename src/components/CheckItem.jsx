import { useState } from "react";

import {
  Typography,
  IconButton,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { UpdateCheckItem } from "./ApiCalls";

const CheckItem = (props) => {
  const { item, state, itemId, deleteCheckItem,cardId} = props;
  const [checked, setChecked] = useState(state==="complete");
  const [error, setError] = useState(null);

  const handleCheckboxChange = (event) => {
    const isChecked = event.target.checked;
    const check = (isChecked ? "complete" : "incomplete")

    UpdateCheckItem(cardId, itemId, check)
      .then((response) => {
        setChecked(isChecked);
        setLoading(false);
        setError(null);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  return (
    <Typography
      component="div"
      sx={{ display: "flex", justifyContent: "space-between" }}
    >
      <FormControlLabel
        control={
          <Checkbox
            checked={checked}
            onChange={handleCheckboxChange}
            inputProps={{ "aria-label": "controlled" }}
          />
        }
        label={item}
      />
      <IconButton
        onClick={() => {
          deleteCheckItem(itemId);
        }}
      >
        <CloseIcon />
      </IconButton>
    </Typography>
  );
};

export default CheckItem;
