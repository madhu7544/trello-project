import { useState, useEffect } from "react";
import Board from "./Board";
import AddBoard from "./AddBoard";
import { getBoards, addBoard } from "./ApiCalls";
import LoadingPage from "./LoadingPage";

const Home = () => {
  const [boards, setBoards] = useState([]);
  const [newBoard, setNewBoard] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    getBoards()
      .then((response) => {
        const boardData = response.data.map((each) => ({
          id: each.id,
          boardName: each.name,
          background: each.prefs.backgroundImage,
        }));
        setBoards(boardData);
        setLoading(false);
        setError(null);
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
      });
  }, []);

  const addNewBoard = () => {
    if(!newBoard){
      return
    }
    addBoard(newBoard)
      .then((response) => {
        const boardData = {
          id: response.data.id,
          boardName: response.data.name,
          background: response.data.backgroundImage,
        };
        setBoards([...boards, boardData]);
      })
      .catch((error) => {
        setError(error.message); 
      });
  };

  const handleBoardChange = (e) => {
    setNewBoard(e.target.value);
  };

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="boards-container">
      {error ? (
        <p>Error: {error}</p>
      ) : (
        <>
          {boards.map((each) => (
            <Board
              key={each.id}
              boardId={each.id}
              name={each.boardName}
              backgroundImage={`url(${each.background})`}
            />
          ))}
          <AddBoard
            newBoardValue={newBoard}
            onBoardChange={handleBoardChange}
            addNewBoard={addNewBoard}
          />
        </>
      )}
    </div>
  );
};

export default Home;
