import React from "react";
import { AppBar, Toolbar, IconButton, Box } from "@mui/material";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <AppBar position="static">
      <Toolbar sx={{ backgroundColor: "#6deddc" }}>
        <Box>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="back"
          ></IconButton>
          <Link to="/boards">
            <img
              src="https://res.cloudinary.com/dsiyffj0o/image/upload/v1686029338/PikPng.com_trello-logo-png_1413730_ikr1wr.png"
              alt="Logo"
              style={{ marginLeft: "10px", height: "30px" }}
            />
          </Link>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
