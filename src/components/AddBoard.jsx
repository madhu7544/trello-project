import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const AddBoard = (props) => {
  const { newBoardValue, onBoardChange, addNewBoard } = props;

  const [expanded, setExpanded] = useState(false);

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  const addNewBoardHome =()=>{
    addNewBoard()
    setExpanded(!expanded);
  }

  return (
    <Card
      sx={{
        margin: "20px",
        width: 300,
        height: 200,
      }}
      variant="outlined"
    >
      <CardContent>
        <Button
          onClick={handleExpand}
          sx={{
            color: "#1345a1",
            fontSize: "18px",
            fontWeight: "bold",
          }}
        >
          + Add Board
        </Button>
        {expanded && (
          <div>
            <TextField
              label="Enter Board Name"
              value={newBoardValue}
              onChange={onBoardChange}
            />
            <Button variant="contained" color="primary" onClick={addNewBoardHome} sx={{marginTop:'10px'}}>
              Add
            </Button>
          </div>
        )}
      </CardContent>
    </Card>
  );
};

export default AddBoard;
