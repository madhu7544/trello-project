import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const AddCheckListItem= (props) => {
  const { newCheckItem, onChekItemChange, addNewCheckItem } = props;

  const [expanded, setExpanded] = useState(false);

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  const addNewCheckItemHome =()=>{
    addNewCheckItem()
    setExpanded(!expanded);
  }

  return (
      <CardContent>
        {!expanded && <Button
          onClick={handleExpand}
          variant="contained"
        >
         Add
        </Button>
}
        {expanded && (
          <div>
            <TextField
              label="CheckList Item"
              value={newCheckItem}
              onChange={onChekItemChange}
            />
            <Button variant="contained" color="primary" onClick={addNewCheckItemHome} sx={{marginLeft:'10px'}}>
              Add
            </Button>
          </div>
        )}
      </CardContent>
  );
};

export default AddCheckListItem;
