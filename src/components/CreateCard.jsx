import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const CreateCard = (props) => {
  const { newCardValue, onCardChange, addNewCard } = props;

  const [expanded, setExpanded] = useState(false);

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ margin: "10px" }} variant="outlined">
      <CardContent>
        <Button
          onClick={handleExpand}
          sx={{
            color: "#1345a1",
          }}
        >
        + Add Card
        </Button>
        {expanded && (
          <div>
            <TextField
              label="Enter Card Name"
              value={newCardValue}
              onChange={onCardChange}
            />
            <Button
              variant="contained"
              color="primary"
              onClick={addNewCard}
              sx={{ marginLeft: "10px" }}
            >
              Add
            </Button>
          </div>
        )}
      </CardContent>
    </Card>
  );
};

export default CreateCard;
