import axios from "axios";
import config from "/config.js";

const apiKey = config.apiKey;
const token = config.token;

//getting boards data
export const getBoards = () => {
  const response = axios.get(
    `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`
  );
  return response;
};

//adding new Board
export const addBoard = (newBoard) => {
  const response = axios.post(
    `https://api.trello.com/1/boards/?name=${newBoard}&key=${apiKey}&token=${token}`
  );
  return response;
};

//getting list of cards
export const getListsCards = (id) => {
  const response = axios.get(
    `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${token}`
  );
  return response;
};

//adding new list
export const addList =  (newList, id) => {
  const response = axios.post(
    `https://api.trello.com/1/lists?name=${newList}&idBoard=${id}&key=${apiKey}&token=${token}`
  );
  return response;
};

//deleting list

export const deleteListItem = (listId) => {
  const response = axios.put(
    `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${token}`
  );
  return response;
};

// Getting cards in a list
export const getCards = (listId) => {
  const response = axios.get(
    `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`
  );
  return response;
};

//Adding cards in a list
export const addCards = (newCard, listId) => {
  const response = axios.post(
    `https://api.trello.com/1/cards?name=${newCard}&idList=${listId}&key=${apiKey}&token=${token}`
  );
  return response;
};

//deleteing card ina list

export const deleteCardItem = (cardId) => {
  const response = axios.delete(
    `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`
  );
  return response;
};

//getting checkList Data

export const CheckListData = (cardId) => {
  const response = axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`
  );
  return response;
};

//adding Checklist in a card

export const AddChecklistItem = (newCheckItem, cardId) => {
  const response = axios.post(
    `https://api.trello.com/1/cards/${cardId}/checklists?name=${newCheckItem}&key=${apiKey}&token=${token}`
  );
  return response;
};

//deleting checkList in a card

export const DeleteChecklistItem = (checkListId) => {
  const response = axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}?key=${apiKey}&token=${token}`
  );
  return response;
};

//getting checkListItem

export const GetChecklistItem = (checkListId) => {
  const response = axios.get(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${token}`
   
  )
  return response;
}


//adding checkList Item

export const AddCheckItem = (checkListId, checkName) => {
  const response = axios.post(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkName}&key=${apiKey}&token=${token}`
  );
  return response;
};

//deleting checkItem

export const DeleteCheckItem = (checkListId, checkItemId) => {
  const response = axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${token}`
  );
  return response;
};

// updating checkItem

export const UpdateCheckItem = (cardId,itemId,checked) => {
  const response = axios.put(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${checked}&key=${apiKey}&token=${token}`
  );
  return response;
};
