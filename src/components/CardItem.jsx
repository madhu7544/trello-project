import { useState, useEffect } from "react";
import {
  Card,
  CardContent,
  Typography,
  IconButton,
  Box,
  Modal,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CloseIcon from "@mui/icons-material/Close";
import {
  CheckListData,
  AddChecklistItem,
  DeleteChecklistItem,
} from "./ApiCalls";

import AddCheckList from "./AddCheckList";
import CheckList from "./CheckLists";
import LoadingPage from "./LoadingPage";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const CardItem = (props) => {
  const { card, deleteCard, listName } = props;
  const [checkLists, setCheckLists] = useState([]);
  const [newCheckList, setNewCheckList] = useState("Checklist");
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    CheckListData(card.id)
      .then((response) => {
        setCheckLists(response.data);
        setLoading(false);
        setError(null);
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
      });
  }, []);

  const addNewCheckList = () => {
    if(!newCheckList){
      return
    }
    AddChecklistItem(newCheckList, card.id)
      .then((response) => {
        setCheckLists([...checkLists, response.data]);
        setNewCheckList("Checklist");
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const onCheckListChange = (e) => {
    setNewCheckList(e.target.value);
  };

  const deleteCheckList = (checkListId) => {
    DeleteChecklistItem(checkListId)
      .then((response) => {
        setCheckLists(
          checkLists.filter((checkList) => checkList.id !== checkListId)
        );
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  return loading ? (
    <LoadingPage />
  ) : (
    <>
      <Card
        sx={{
          margin: "10px",
          padding: "10px",
          display: "flex",
          justifyContent: "space-between",
        }}
        onClick={handleOpen}
      >
        <Typography>{card.name}</Typography>
        <IconButton
          onClick={(e) => {
            e.stopPropagation();
            deleteCard(card.id);
          }}
        >
          <DeleteIcon />
        </IconButton>
      </Card>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <CardContent
            sx={{ display: "flex", justifyContent: "space-between" }}
          >
            <Typography marginLeft="10px" sx={{ fontSize: "20px" }}>
              {card.name} in list {listName}
            </Typography>
            <IconButton onClick={handleClose}>
              <CloseIcon />
            </IconButton>
          </CardContent>
          <CardContent sx={{ margin: "10px" }}>
            {checkLists.map((each) => (
              <CheckList
                key={each.id}
                checkList={each}
                deleteCheckList={deleteCheckList}
                cardId={card.id}
              />
            ))}
            {error ? (
              <p>Error: {error}</p>
            ) : (
              <AddCheckList
                newCheckListValue={newCheckList}
                onCheckListChange={onCheckListChange}
                addNewCheckList={addNewCheckList}
              />
            )}
          </CardContent>
        </Box>
      </Modal>
    </>
  );
};

export default CardItem;
