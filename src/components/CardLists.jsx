import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import ListOfCards from "./ListOfCards";
import CreateList from "./CreateList";
import { getListsCards, addList, deleteListItem } from "./ApiCalls";
import { Card } from "@mui/material";
import LoadingPage from "./LoadingPage";

const CardLists = () => {
  const { id } = useParams();
  const [lists, setLists] = useState([]);
  const [newList, setList] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    getListsCards(id)
      .then((response) => {
        setLists(response.data);
        setLoading(false);
        setError(null);
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
      });
  }, []);

  const addNewList = () => {
    if(!newList){
      return
    }
    addList(newList, id)
      .then((response) => {
        setLists([...lists, response.data]);
        setList("");
      })
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  };

  const handleListChange = (e) => {
    setList(e.target.value);
  };

  const deleteList = (listId) => {
    deleteListItem(listId)
      .then((response) => {
        setLists(lists.filter((list) => list.id !== listId));
      })
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  };

  return (
  <div className="lists-container">
    {loading ? (
      <LoadingPage />
    ) : (
      <>
        {error ? (
          <p>Error: {error}</p>
        ) : (
          <>
            {lists.map((each) => (
              <ListOfCards
                key={each.id}
                listId={each.id}
                name={each.name}
                deleteList={deleteList}
              />
            ))}
            <CreateList
              newListValue={newList}
              onListChange={handleListChange}
              addNewList={addNewList}
            />
          </>
        )}
      </>
    )}
  </div>
);
};
export default CardLists;
