import { useState, useEffect } from "react";
import { Card, CardContent, Typography, IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import LibraryAddCheckIcon from "@mui/icons-material/LibraryAddCheck";
import AddChecklistItem from "./AddCheckListItem";
import { GetChecklistItem, AddCheckItem, DeleteCheckItem } from "./ApiCalls";
import CheckItem from "./CheckItem";
import LoadingPage from "./LoadingPage";

const CheckList = (props) => {
  const { checkList, deleteCheckList, cardId } = props;
  const [checkItems, setCheckItems] = useState([]);
  const [newCheckItem, setNewCheckItem] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    GetChecklistItem(checkList.id)
      .then((response) => {
        setCheckItems(response.data);
        setLoading(false);
        setError(null);
      })
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  }, []);

  const addNewCheckItem = () => {
    AddCheckItem(checkList.id, newCheckItem)
      .then((response) => {
        setCheckItems([...checkItems, response.data]);
        setNewCheckItem("");
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const deleteCheckItem = (checkItemId) => {
    DeleteCheckItem(checkList.id, checkItemId)
      .then((response) => {
        setCheckItems(checkItems.filter((each) => each.id !== checkItemId));
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const onChekItemChange = (e) => {
    setNewCheckItem(e.target.value);
  };

  return  (
    <Card>
      <CardContent
        sx={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Typography component="div" sx={{ display: "flex" }}>
          <LibraryAddCheckIcon />
          <Typography sx={{ ml: 2 }}>{checkList.name}</Typography>
        </Typography>
        <IconButton onClick={() => deleteCheckList(checkList.id)}>
          <DeleteIcon />
        </IconButton>
      </CardContent>
      {loading ? (
    <LoadingPage />
  ) :
      <CardContent sx={{ marginLeft: "15px", marginRight: "15px" }}>
        {checkItems.map((item) => (
          <CheckItem
            key={item.id}
            itemId={item.id}
            item={item.name}
            state={item.state}
            deleteCheckItem={deleteCheckItem}
            cardId = {cardId}
          />
        ))}
        {error ? (
          <p>Error: {error}</p>
        ) : (
          <AddChecklistItem
            newCheckItem={newCheckItem}
            onChekItemChange={onChekItemChange}
            addNewCheckItem={addNewCheckItem}
          />
        )}
      </CardContent>}
    </Card>
  );
};

export default CheckList;
