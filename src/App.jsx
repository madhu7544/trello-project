import { useState } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import CardLists from "./components/CardLists";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Navigate to="/boards" />} />
        <Route path="/boards" element={<Home />} />
        <Route path="/boards/:id" element={<CardLists />} />
      </Routes>
    </>
  );
}

export default App;
